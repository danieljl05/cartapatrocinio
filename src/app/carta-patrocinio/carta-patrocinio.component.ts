import { Component, OnInit } from "@angular/core";
import { Letter } from "../models/Letter";
import { LetterService } from "../letter.service";

@Component({
  selector: "app-carta-patrocinio",
  templateUrl: "./carta-patrocinio.component.html",
  styleUrls: ["./carta-patrocinio.component.css"]
})
export class CartaPatrocinioComponent implements OnInit {
  public letter: Letter;
  public receptors: Object[];
  public languages: Object[];
  public letters: Object[];
  public currentLetters: Object[];

  constructor(private letterService: LetterService) {
    this.letter = new Letter();
  }

  ngOnInit() {
    this.letter.language = 1;
    this.letter.person = 1;
    this.receptors = [
      {
        id: 1,
        name: "A quien pueda interesar"
      },
      {
        id: 2,
        name: "A esta persona"
      }
    ];

    this.languages = [
      {
        id: 1,
        name: "Español"
      },
      {
        id: 2,
        name: "Inglés"
      },
      {
        id: 3,
        name: "Francés"
      },
      {
        id: 4,
        name: "Italiano"
      },
      {
        id: 5,
        name: "Alemán"
      }
    ];

    this.letters = [
      {
        id: 1,
        list: [
          "Embajada con presupuesto",
          "Embajada y otros sin presupuesto",
          "Universidades y otros",
          "Universidades y otros (EUR)",
          "Extensión de estudios"
        ]
      },
      {
        id: 2,
        list: [
          "Embassy with budget",
          "Embassy and others without a budget",
          "Universities and others",
          "Universities and others (EUR)",
          "Study extension"
        ]
      },
      {
        id: 3,
        list: [
          "Ambassade avec budget",
          "Ambassade et autres sans budget",
          "Universités et autres",
          "Universités et autres (EUR)",
          "Extension de l'étude"
        ]
      },
      {
        id: 4,
        list: [
          "Ambasciata con budget",
          "Ambasciata e altri senza un budget",
          "Università e altri",
          "Università e altri (EUR)",
          "Estensione di studio"
        ]
      },
      {
        id: 5,
        list: [
          "Botschaft mit Budget",
          "Botschaft und andere ohne Budget",
          "Universitäten und andere",
          "Universitäten und andere (EUR)",
          "Studienerweiterung"
        ]
      }
    ];
    this.onChangeLanguage();
  }

  onChangeLanguage() {
    console.log(this.letter.language);
    this.currentLetters = this.letters.filter(
      language => language["id"] === this.letter.language
    );
  }

  getPdf() {
    this.letterService.getPdf(this.letter);
  }
}
