import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartaPatrocinioComponent } from './carta-patrocinio.component';

describe('CartaPatrocinioComponent', () => {
  let component: CartaPatrocinioComponent;
  let fixture: ComponentFixture<CartaPatrocinioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartaPatrocinioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartaPatrocinioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
