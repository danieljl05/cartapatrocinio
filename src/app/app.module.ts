import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { CartaPatrocinioComponent } from './carta-patrocinio/carta-patrocinio.component';
import { ReceptorFormComponent } from './receptor-form/receptor-form.component';

@NgModule({
  declarations: [
    AppComponent,
    CartaPatrocinioComponent,
    ReceptorFormComponent
  ],
  imports: [
    BrowserModule,
    CoreModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
