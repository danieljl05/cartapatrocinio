import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceptorFormComponent } from './receptor-form.component';

describe('ReceptorFormComponent', () => {
  let component: ReceptorFormComponent;
  let fixture: ComponentFixture<ReceptorFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceptorFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceptorFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
