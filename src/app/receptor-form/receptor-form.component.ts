import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-receptor-form',
  templateUrl: './receptor-form.component.html',
  styleUrls: ['./receptor-form.component.css']
})
export class ReceptorFormComponent implements OnInit {

  @ViewChild("f") slForm: NgForm;
  constructor() { }

  ngOnInit() {
  }

}
