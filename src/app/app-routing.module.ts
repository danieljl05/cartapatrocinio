import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CartaPatrocinioComponent } from "./carta-patrocinio/carta-patrocinio.component";

const routes: Routes = [
  { path: "", redirectTo: "carta-patrocinio", pathMatch: "full" },
  { path: "carta-patrocinio", component: CartaPatrocinioComponent },
  { path: "**", redirectTo: "carta-patrocinio" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
